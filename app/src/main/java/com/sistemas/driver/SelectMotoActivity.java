package com.sistemas.driver;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sistemas.driver.util.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SelectMotoActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private String access_token;
    private SharedPreferences sharedPreferences;
    private LinearLayout llVehiculos;
    private ProgressBar loader;
    private int user_id;

    private int dia_numero;
    private int dia_semana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setTitle("Seleccione un vehículo");
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() == 0) {
            goMain();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_moto);

        requestQueue = Volley.newRequestQueue(this);

        user_id = sharedPreferences.getInt("user_id", 0);

        llVehiculos = (LinearLayout) findViewById(R.id.llVehiculos);
        loader = (ProgressBar) findViewById(R.id.progressBar);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd");
        dia_numero = Integer.parseInt(mdformat.format(calendar.getTime()));

        mdformat.applyPattern("u");
        dia_semana = Integer.parseInt(mdformat.format(calendar.getTime()));

        loadMotos();
    }

    public void loadMotos() {
        final Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/users/" + user_id + "/vehiculos";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            if (response.length() > 0) {
                                loader.setVisibility(View.GONE);
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject v = response.getJSONObject(i);
                                    LayoutInflater inflater = (LayoutInflater) SelectMotoActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    final View ll = inflater.inflate(R.layout.vehiculo, null);
                                    final int id = v.getInt("id");
                                    final String placa = v.getString("placa");
                                    String marca = v.getString("marca");
                                    String modelo = v.getString("modelo");
                                    String color = v.getString("color");
                                    ((TextView) ll.findViewById(R.id.tvPlaca)).setText(placa);
                                    ((TextView) ll.findViewById(R.id.tvInfo)).setText(marca + " " + modelo + " " + color);
                                    if (isPicoYPlaca(placa)) {
                                        ((TextView) ll.findViewById(R.id.tvPlaca)).setTextColor(resources.getColor(R.color.btnMotoPicoYPlaca, null));
                                        ll.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {
                                                Toast.makeText(SelectMotoActivity.this, "La moto se encuentra en pico y placa.", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } else {
                                        ll.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                selectMoto(id, placa);
                                            }
                                        });
                                    }
                                    ImageButton ib = (ImageButton) ll.findViewById(R.id.btnEditar);
                                    ib.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(SelectMotoActivity.this, AddOrEditVehiculoActivity.class);
                                            intent.putExtra("vehiculo_id", id);
                                            startActivity(intent);
                                        }
                                    });

                                    llVehiculos.addView(ll);
                                }
                            } else {
                                Toast.makeText(SelectMotoActivity.this, "No tienes vehiculos registrados.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    private boolean isPicoYPlaca(String placa) {
        // Formato placa ABC 12D = 7 caracteres
        if (placa.length() == 7 && dia_semana < 6) {
            int placa_numero = Integer.parseInt(placa.substring(4, 6));
            // Si es par o impar al igual que hoy
            return placa_numero % 2 != dia_numero % 2;
        }
        return false;
    }

    public void selectMoto(int moto_id, String placa) {
        Intent intent = new Intent(this, GoogleMapsActivity.class);
        //intent.putExtra("moto_id", moto_id);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("moto_id", moto_id);
        editor.putString("placa", placa);
        editor.commit();
        startActivity(intent);
    }

    public void goMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void goAddOrEditVehiculo(View view) {
        Intent intent = new Intent(this, AddOrEditVehiculoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Helper.alert(this, "Salir", "¿Seguro que deseas salir de la aplicación?", "Salir", "Cerrar", new Helper.ResponseConfirm() {
            @Override
            public void onAccept(DialogInterface dialog, int id) {
                SelectMotoActivity.this.finishAffinity();
            }

            @Override
            public void onCancel(DialogInterface dialog, int id) {
                // No pasa nada :)
            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
