package com.sistemas.driver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class EditVehiculoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_vehiculo);
    }

    public void regresar(View view) {
        finish();
    }

    public void actualizar(View view) {
        Intent intent = new Intent(this, SelectMotoActivity.class);
        startActivity(intent);
    }
}
