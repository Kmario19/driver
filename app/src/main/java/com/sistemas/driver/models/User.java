package com.sistemas.driver.models;

public class User {

    private int id;
    private String identificacion;
    private String nombres;
    private String email;
    private String celular;
    private String tipo;
    private String activo;
    private Double lat;
    private Double lng;
    private int vehiculo_id;
    private Vehiculo vehiculo;

    public User() {
    }

    public User(int id, String identificacion, String nombres, String email, String celular, String tipo, String activo, Double lat, Double lng, int vehiculo_id, Vehiculo vehiculo) {
        this.id = id;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.email = email;
        this.celular = celular;
        this.tipo = tipo;
        this.activo = activo;
        this.lat = lat;
        this.lng = lng;
        this.vehiculo_id = vehiculo_id;
        this.vehiculo = vehiculo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public int getVehiculoId() {
        return vehiculo_id;
    }

    public void setVehiculoId(int vehiculo_id) {
        this.vehiculo_id = vehiculo_id;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }
}
