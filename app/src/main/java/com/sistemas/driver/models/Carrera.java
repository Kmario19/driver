package com.sistemas.driver.models;

import com.google.android.gms.maps.model.MarkerOptions;

public class Carrera {

    private int id;
    private int user_id;
    private int conductor_id;
    private double origen_lat;
    private double origen_lng;
    private String origen_dir;
    private double destino_lat;
    private double destino_lng;
    private String destino_dir;
    private int precio;
    private int calificacion;
    private String estado;
    private MarkerOptions origen_marker;
    private MarkerOptions destino_marker;
    private User user;

    public Carrera() {
    }

    public Carrera(int id, int user_id, int conductor_id, double origen_lat, double origen_lng, String origen_dir, double destino_lat, double destino_lng, String destino_dir, int precio, int calificacion, String estado) {
        this.id = id;
        this.user_id = user_id;
        this.conductor_id = conductor_id;
        this.origen_lat = origen_lat;
        this.origen_lng = origen_lng;
        this.origen_dir = origen_dir;
        this.destino_lat = destino_lat;
        this.destino_lng = destino_lng;
        this.destino_dir = destino_dir;
        this.precio = precio;
        this.calificacion = calificacion;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getConductorId() {
        return conductor_id;
    }

    public void setConductorId(int conductor_id) {
        this.conductor_id = conductor_id;
    }

    public double getOrigenLat() {
        return origen_lat;
    }

    public void setOrigenLat(double origen_lat) {
        this.origen_lat = origen_lat;
    }

    public double getOrigenLng() {
        return origen_lng;
    }

    public void setOrigenLng(double origen_lng) {
        this.origen_lng = origen_lng;
    }

    public String getOrigenDir() {
        return origen_dir;
    }

    public void setOrigenDir(String origen_dir) {
        this.origen_dir = origen_dir;
    }

    public double getDestinoLat() {
        return destino_lat;
    }

    public void setDestinoLat(double destino_lat) {
        this.destino_lat = destino_lat;
    }

    public double getDestinoLng() {
        return destino_lng;
    }

    public void setDestinoLng(double destino_lng) {
        this.destino_lng = destino_lng;
    }

    public String getDestinoDir() {
        return destino_dir;
    }

    public void setDestinoDir(String destino_dir) {
        this.destino_dir = destino_dir;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public MarkerOptions getOrigenMarker() {
        return origen_marker;
    }

    public void setOrigenMarker(MarkerOptions origen_marker) {
        this.origen_marker = origen_marker;
    }

    public MarkerOptions getDestinoMarker() {
        return destino_marker;
    }

    public void setDestinoMarker(MarkerOptions destino_marker) {
        this.destino_marker = destino_marker;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carrera carrera = (Carrera) o;
        return id == carrera.id;
    }
}
