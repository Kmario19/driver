package com.sistemas.driver;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.sistemas.driver.util.AjaxRequest;
import com.sistemas.driver.util.Helper;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class AddOrEditVehiculoActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private String access_token;
    private SharedPreferences sharedPreferences;
    private AjaxRequest ajaxRequest;

    private int vehiculo_id;
    private int user_id;

    private Button btnAction;
    private ImageButton btnEliminar;
    private EditText etPlaca;
    private EditText etMarca;
    private EditText etModelo;
    private EditText etColor;
    private TextView tvTitulo;
    private ProgressBar loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        user_id = sharedPreferences.getInt("user_id", 0);

        if (access_token.length() == 0 || user_id == 0) {
            goMain();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_vehiculo);

        requestQueue = Volley.newRequestQueue(this);
        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        btnAction = (Button) findViewById(R.id.btnAction);
        btnEliminar = (ImageButton) findViewById(R.id.btnBorrar);
        etPlaca = (EditText) findViewById(R.id.etPlaca);
        etMarca = (EditText) findViewById(R.id.etMarca);
        etModelo = (EditText) findViewById(R.id.etModelo);
        etColor = (EditText) findViewById(R.id.etColor);
        tvTitulo = (TextView) findViewById(R.id.tvTitulo);
        loader = (ProgressBar) findViewById(R.id.progressBar);

        vehiculo_id = getIntent().getIntExtra("vehiculo_id", 0);
        if (vehiculo_id == 0) {
            btnAction.setText("Registrar");
            btnEliminar.setVisibility(View.INVISIBLE);
            tvTitulo.setText("Registrar vehículo");
        } else {
            btnAction.setText("Guardar");
            btnEliminar.setVisibility(View.VISIBLE);
            tvTitulo.setText("Editar vehículo");
            loadInfo();
        }
    }

    private void loadInfo() {
        String url = getResources().getString(R.string.API_URL) + "/api/vehiculos/" + vehiculo_id;
        ajaxRequest.get(url, null, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    etPlaca.setText(response.getString("placa"));
                    etMarca.setText(response.getString("marca"));
                    etModelo.setText(response.getString("modelo"));
                    etColor.setText(response.getString("color"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void goMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void clickAction(View view) {
        String placa = etPlaca.getText().toString();
        String marca = etMarca.getText().toString();
        String modelo = etModelo.getText().toString();
        String color = etColor.getText().toString();
        if (placa.length() == 0 || marca.length() == 0 || modelo.length() == 0 || color.length() == 0) {
            Toast.makeText(this, "Complete todos los datos", Toast.LENGTH_SHORT).show();
            return;
        }
        loader.setVisibility(View.VISIBLE);
        if (vehiculo_id == 0) {
            registrar(placa, marca, modelo, color);
        } else {
            actualizar(placa, marca, modelo, color);
        }
    }

    private void actualizar(String placa, String marca, String modelo, String color) {
        String url = getResources().getString(R.string.API_URL) + "/api/vehiculos/" + vehiculo_id;
        Map<String, String> params = new HashMap<String, String>();
        params.put("placa", placa);
        params.put("marca", marca);
        params.put("modelo", modelo);
        params.put("color", color);
        ajaxRequest.put(url, params, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Toast.makeText(AddOrEditVehiculoActivity.this, "Datos actualizados", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void registrar(String placa, String marca, String modelo, String color) {
        String url = getResources().getString(R.string.API_URL) + "/api/vehiculos";
        Map<String, String> params = new HashMap<String, String>();
        params.put("placa", placa);
        params.put("marca", marca);
        params.put("modelo", modelo);
        params.put("color", color);
        params.put("user_id", user_id+"");
        ajaxRequest.post(url, params, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Toast.makeText(AddOrEditVehiculoActivity.this, "Vehículo registrado con éxito", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public void delete(View view) {
        Helper.alert(this, "Eliminar vehículo", "¿Estás seguro de eliminar este vehículo?", "Si", "No", new Helper.ResponseConfirm() {
            @Override
            public void onCancel(DialogInterface dialog, int id) {
                dialog.cancel();
            }

            @Override
            public void onAccept(DialogInterface dialog, int id) {
                loader.setVisibility(View.VISIBLE);
                String url = getResources().getString(R.string.API_URL) + "/api/vehiculos/" + vehiculo_id;
                ajaxRequest.delete(url, new AjaxRequest.ResponseObject() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Toast.makeText(AddOrEditVehiculoActivity.this, "Vehículo eliminado con éxito", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
    }
}
