package com.sistemas.driver;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MapActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private SharedPreferences sharedPreferences;

    private int moto_id;
    private int user_id;
    private String placa;
    private String access_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        moto_id = sharedPreferences.getInt("moto_id", 0);
        placa = sharedPreferences.getString("placa", "NN");
        user_id = sharedPreferences.getInt("user_id", 0);
        access_token = sharedPreferences.getString("access_token", "");

        if (moto_id == 0 || placa.equals("NN") || user_id == 0 || access_token.length() == 0) {
            Toast.makeText(this, "No se hay ids en esta sesión.", Toast.LENGTH_SHORT).show();
            return;
        }

        loadMotoInfo();

    }

    public void loadMotoInfo() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/vehiculos/" + moto_id;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String placa = response.getString("placa");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
}
