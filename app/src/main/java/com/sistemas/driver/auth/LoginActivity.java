package com.sistemas.driver.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sistemas.driver.EditVehiculoActivity;
import com.sistemas.driver.R;
import com.sistemas.driver.SelectMotoActivity;
import com.sistemas.driver.util.AjaxRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private EditText etEmail;
    private EditText etPass;
    private String access_token;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() > 0) {
            goNext();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPass = (EditText) findViewById(R.id.etPass);
        requestQueue = Volley.newRequestQueue(this);

        etEmail.setText(getIntent().getStringExtra("email"));
    }

    public void regresar(View view) {
        finish();
    }

    public void login(View view) {
        String email = etEmail.getText().toString();
        String pass = etPass.getText().toString();
        
        if (email.length() == 0 || pass.length() == 0) {
            Toast.makeText(this, "Complete los datos", Toast.LENGTH_SHORT).show();
            return;
        }

        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/oauth/token";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("username", email);
        jsonParams.put("password", pass);
        jsonParams.put("grant_type", "password");
        jsonParams.put("client_id", "2");
        jsonParams.put("client_secret", "Hebes8GZVpR3fJ401eXa9hDWXmys5TvCqgyBOOOs");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token_type = response.getString("token_type");
                            if (token_type.equals("Bearer")) {
                                access_token = response.getString("access_token");
                                String refresh_token = response.getString("refresh_token");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("access_token", access_token);
                                editor.putString("refresh_token", refresh_token);
                                editor.putString("token_type", token_type);
                                editor.commit();
                                Toast.makeText(LoginActivity.this, "Ingreso exitoso", Toast.LENGTH_SHORT).show();
                                getUser();
                            } else {
                                Toast.makeText(LoginActivity.this, "El tipo de token es incorrecto: " + token_type, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode == 401) {
                            Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrectas.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this, "Ocurrió un error, intente de nuevo.", Toast.LENGTH_SHORT).show();
                        }
                        error.printStackTrace();
                    }   
                });

        requestQueue.add(request);

        /*
        username:kmario@gmail.com
        password:admin
        grant_type:password
        client_id:2
        client_secret:Hebes8GZVpR3fJ401eXa9hDWXmys5TvCqgyBOOOs

        EL OTRO

        Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJjMjMxZmFiMWI1ODJkZmMwNzMwNGZmOGVmYTc5OTQwYzJmODliMDFmNWNmZmMzYjQ3YzU5ZGMxYWI4MGNjZjRhNzM2NzU3NTQ0M2M5MTE5In0.eyJhdWQiOiIyIiwianRpIjoiMmMyMzFmYWIxYjU4MmRmYzA3MzA0ZmY4ZWZhNzk5NDBjMmY4OWIwMWY1Y2ZmYzNiNDdjNTlkYzFhYjgwY2NmNGE3MzY3NTc1NDQzYzkxMTkiLCJpYXQiOjE1MjYzNTk4NjgsIm5iZiI6MTUyNjM1OTg2OCwiZXhwIjoxNTU3ODk1ODY4LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.Et0fG1QBjP7JN4DnQ-5cJ5Vuis0WuSu8MhXVyBE_rTh7fFmqzbjVKCqrLdeplptnVVwfOHUNCULkxdMtlL5soIW0AfcOc96G-GZKlK6tpLykkIs3lvycDLRBimRD-Ex95nJsxit8_EhQLA8xdL3mO4Ka93Io2n9M1rzFiccS6cTxlNYgmuhkNLdr90jl1xDTnmQXWKI1yByviq8z9MumjHhvQhD8MidKjKj0_9kIxE5HSWOCckRdBCBTxf2jr4JL95LojSNFOhwE8WMVLeYCagNg3Lg1IwW5gGK4lgsYioa1gN_MZBLzv-YO9mpPPQLe1pQ3HBjw6SP3RwdX5yRtu2vK9ETTQSj3Nm4Epal4bDpSSoiG48uLTIFx6k5cIesZnMVqOE6sENMAbqLytW1UHFRBcsvqg3W7-A4NWhgwQG5Qzxq9EOWUpQ5VVtcEUR24aDp99eEGTQm4eCeZMJiXS7-nglMOLmpCmZISo4mQwaDbA8aMwENqAs1jbaqjyKd0VW1QSL0ABFv8yGwtjPmOx4YYZe5iKAhinh0sCmuqwioW-xDUJ5RGLebXJAIs6SH1-N7lBUuQe9KEWf15xMuaTjSNSoAd2q6xeHbNf-s_PoUXMHBVlSo7w9ccSQmT-L5R23P7ZATPGweb38n0ycSQrDvh-HxbKMzucSRFxUEd2Lw
        Accept:application/json
        */
    }

    private void goNext() {
        Intent intent;
        String editado = sharedPreferences.getString("editado", "");
        if (editado.equals("SI") || true) {
            intent = new Intent(this, SelectMotoActivity.class);
        } else {
            intent = new Intent(this, EditVehiculoActivity.class);
        }
        startActivity(intent);
    }

    private void getUser() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/user";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int user_id = response.getInt("id");
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt("user_id", user_id);
                            editor.commit();
                            goNext();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
}
