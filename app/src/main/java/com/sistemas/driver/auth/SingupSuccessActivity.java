package com.sistemas.driver.auth;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.sistemas.driver.EditVehiculoActivity;
import com.sistemas.driver.R;

public class SingupSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup_success);
    }

    public void inicio(View view) {
        Intent intent = new Intent(this, EditVehiculoActivity.class);
        startActivity(intent);
    }
}
