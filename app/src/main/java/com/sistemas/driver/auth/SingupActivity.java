package com.sistemas.driver.auth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sistemas.driver.R;
import com.sistemas.driver.util.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SingupActivity extends AppCompatActivity {

    private RequestQueue requestQueue;
    private EditText etEmail;
    private EditText etNombres;
    private EditText etId;
    private EditText etCelular;
    private EditText etPlaca;
    private SharedPreferences sharedPreferences;
    private String access_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etNombres= (EditText) findViewById(R.id.etNombres);
        etId = (EditText) findViewById(R.id.etId);
        etCelular = (EditText) findViewById(R.id.etCelular);
        etPlaca = (EditText) findViewById(R.id.etPlaca);

        requestQueue = Volley.newRequestQueue(this);
    }

    public void regresar(View view) {
        finish();
    }

    public void registro(View view) {
        String nombres = etNombres.getText().toString();
        String id = etId.getText().toString();
        String email = etEmail.getText().toString();
        String celular = etCelular.getText().toString();
        String tipo = "C"; // Conductor
        String placa = etPlaca.getText().toString();

        if (nombres.length() == 0 || email.length() == 0 || id.length() == 0 || email.length() == 0 || celular.length() == 0
                || placa.length() == 0) {
            Toast.makeText(this, "Complete los datos", Toast.LENGTH_SHORT).show();
            return;
        }

        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/register";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("nombres", nombres);
        jsonParams.put("email", email);
        jsonParams.put("celular", celular);
        jsonParams.put("tipo", tipo);
        jsonParams.put("placa", placa);
        jsonParams.put("identificacion", id);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean success = response.getBoolean("success");
                            if (success) {
                                JSONObject data = response.getJSONObject("data");
                                access_token = data.getString("token");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("access_token", access_token);
                                editor.commit();
                                Toast.makeText(SingupActivity.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                                getUser();
                            } else {
                                String message = response.getString("message");
                                Toast.makeText(SingupActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(SingupActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.statusCode == 401) {
                    Toast.makeText(SingupActivity.this, "Ocurrió un error, intente de nuevo.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SingupActivity.this, "Ocurrió un error con la solicitud, intente de nuevo.", Toast.LENGTH_SHORT).show();
                }
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        requestQueue.add(request);

    }

    private void goNext() {
        Helper.alert(this,
                "¡Registro Exitoso!",
                "Se le ha enviado un código para ingresar y usar el servicio de UberMot.",
                "Aceptar", new Helper.ResponseAlert() {
                    @Override
                    public void onAccept(DialogInterface dialog, int id) {
                        Intent intent = new Intent(SingupActivity.this, LoginActivity.class);
                        intent.putExtra("email", etEmail.getText().toString());
                        startActivity(intent);
                    }
                });
    }

    private void getUser() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/user";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int user_id = response.getInt("id");
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt("user_id", user_id);
                            editor.commit();
                            goNext();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }
}
