package com.sistemas.driver.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Helper {

    public static MarkerOptions createMarker(double lat, double lng) {
        return new MarkerOptions().position(new LatLng(lat, lng));
    }

    public static MarkerOptions createMarker(double lat, double lng, String title) {
        return createMarker(lat, lng).title(title);
    }

    public static MarkerOptions createMarker(double lat, double lng, int marker_id) {
        return createMarker(lat, lng).icon(BitmapDescriptorFactory.fromResource(marker_id));
    }

    public static MarkerOptions createMarker(double lat, double lng, String title, int marker_id) {
        return createMarker(lat, lng, marker_id).title(title);
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    private String distance(LatLng origen, LatLng destino) {
        double theta = origen.longitude - destino.longitude;
        double dist = Math.sin(deg2rad(origen.longitude))
                * Math.sin(deg2rad(destino.longitude))
                * Math.cos(deg2rad(origen.longitude))
                * Math.cos(deg2rad(destino.longitude))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return new DecimalFormat("#.#").format(dist) + " Km";
    }

    private double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    /**
     * Method to decode polyline
     * Source : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    public static List decodePolyline(String encoded) {

        List poly = new ArrayList();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public static void expand(final View v) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getLayoutParams().height;

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private static AlertDialog.Builder constructAlert(Context context, String titulo, String body, String btnAceptar, final ResponseAlert callbackAlert) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(body);
        alertDialog.setPositiveButton(btnAceptar, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                callbackAlert.onAccept(dialog, id);
            }
        });
        return alertDialog;
    }

    public static void alert (Context context, String titulo, String body, String btnAceptar, ResponseAlert callbackAlert) {
        AlertDialog.Builder alertDialog = constructAlert(context, titulo, body, btnAceptar, callbackAlert);
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public static void alert (Context context, String titulo, String body, String btnAceptar, String btnCancelar, final ResponseConfirm callbackAlert){
        AlertDialog.Builder alertDialog = constructAlert(context, titulo, body, btnAceptar, callbackAlert);
        alertDialog.setNegativeButton(btnCancelar, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                callbackAlert.onCancel(dialog, id);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public interface ResponseAlert {
        void onAccept(DialogInterface dialog, int id);
    }

    public interface ResponseConfirm extends ResponseAlert {
        void onCancel(DialogInterface dialog, int id);
    }

}
