package com.sistemas.driver.util;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class MyLocation {
    private Location location;
    private LatLng latLng;

    public MyLocation() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = new Location("");
        this.location.setLongitude(location.getLongitude());
        this.location.setLatitude(location.getLatitude());
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public LatLng getLatLng() {
        return latLng;
    }
}
