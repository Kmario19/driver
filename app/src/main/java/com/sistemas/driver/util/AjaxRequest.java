package com.sistemas.driver.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AjaxRequest {

    private RequestQueue requestQueue;
    private Context context;
    private String access_token;

    public AjaxRequest(Context context, RequestQueue requestQueue, String access_token) {
        this.context = context;
        this.access_token = access_token;
        this.requestQueue = requestQueue;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public void get(String url, Map params, ResponseObject responseCallback) {
        send(Request.Method.GET, url, params, responseCallback);
    }

    public void get(String url, Map params, ResponseArray responseCallback) {
        send(Request.Method.GET, url, params, responseCallback);
    }

    public void post(String url, Map params, ResponseObject responseCallback) {
        send(Request.Method.POST, url, params, responseCallback);
    }

    public void put(String url, Map params, ResponseObject responseCallback) {
        params.put("_method", "PUT");
        send(Request.Method.POST, url, params, responseCallback);
    }

    public void delete(String url, ResponseObject responseCallback) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("_method", "DELETE");
        send(Request.Method.POST, url, params, responseCallback);
    }

    private void send(int method, String url, Map params, final ResponseObject responseCallback) {
        JSONObject send_params = params == null ? null : new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(method, url, send_params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean success = response.getBoolean("success");
                            if (success) {
                                responseCallback.onSuccess(response.getJSONObject("data"));
                                Log.i("ajax", response.getString("message"));
                            } else {
                                Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error al parsear JSON", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Toast.makeText(context, networkResponse.statusCode + ": Ocurrió un error, intente de nuevo.", Toast.LENGTH_SHORT).show();
                }
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    private void send(int method, String url, Map params, final ResponseArray responseCallback) {
        JSONObject send_params = params == null ? null : new JSONObject(params);
        JsonObjectRequest request = new JsonObjectRequest(method, url, send_params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean success = response.getBoolean("success");
                            if (success) {
                                responseCallback.onSuccess(response.getJSONArray("data"));
                                Log.i("ajax", response.getString("message"));
                            } else {
                                Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error al parsear JSON", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Toast.makeText(context, networkResponse.statusCode + ": Ocurrió un error, intente de nuevo.", Toast.LENGTH_SHORT).show();
                }
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    public interface ResponseObject {
        void onSuccess(JSONObject response);
    }
    public interface ResponseArray {
        void onSuccess (JSONArray response);
    }
}