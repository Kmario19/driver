package com.sistemas.driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sistemas.driver.models.Carrera;
import com.sistemas.driver.models.User;
import com.sistemas.driver.util.AjaxRequest;
import com.sistemas.driver.util.Helper;
import com.sistemas.driver.util.MyLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class GoogleMapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    private RequestQueue requestQueue;
    private AjaxRequest ajaxRequest;
    private SharedPreferences sharedPreferences;

    private int moto_id;
    private int user_id;
    private String placa;
    private String access_token;

    private MarkerOptions marker_driver;
    private Marker marker_destino;
    private Marker selected_marker;
    private Carrera selected_carrera;
    private Map<Integer, Marker> user_markers;
    private Map<Integer, Carrera> carreras;
    private Polyline ruta_origen;
    private Polyline ruta_destino;
    private MyLocation myLocation;

    private TextView tvPlaca;
    private TextView tvOrigen;
    private TextView tvDestino;
    private TextView tvRecorrido;
    private TextView tvPrecio;
    private TextView tvUserNombre;
    private TextView tvUserTelefono;
    private RelativeLayout rlCarrera;
    private RelativeLayout rlCAccepted;
    private Button btnAceptar;
    private Button btnCancelar;
    private Switch disponible;
    private ImageView pacman;

    private final static int LOCATION_REQUEST = 500;

    private boolean pause_interval = false;
    private boolean zoom_bounds = true;
    private boolean en_carrera = false;
    private boolean con_pasajero = false;
    private LatLngBounds.Builder builder;

    Locale locale;
    NumberFormat currencyFormatter;

    Handler handler;

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() == 0) {
            goMain();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);

        moto_id = sharedPreferences.getInt("moto_id", 0);
        placa = sharedPreferences.getString("placa", "NN");
        user_id = sharedPreferences.getInt("user_id", 0);
        access_token = sharedPreferences.getString("access_token", "");

        if (moto_id == 0 || placa.equals("NN") || user_id == 0 || access_token.length() == 0) {
            Toast.makeText(this, "No se hay ids en esta sesión.", Toast.LENGTH_SHORT).show();
            return;
        }

        tvPlaca = findViewById(R.id.tvPlaca);
        tvOrigen = findViewById(R.id.tvOrigen);
        tvDestino =  findViewById(R.id.tvDestino);
        tvRecorrido = findViewById(R.id.tvRecorrido);
        tvPrecio = findViewById(R.id.tvPrecio);
        rlCarrera = findViewById(R.id.rlCarrera);
        //rlCAccepted = (RelativeLayout) findViewById(R.id.rlCAccepted);
        disponible = findViewById(R.id.switch1);
        btnAceptar = findViewById(R.id.btnAceptarCarrera);
        btnCancelar = findViewById(R.id.btnCancelarCarrera);
        pacman = findViewById(R.id.pacman);
        tvUserNombre = findViewById(R.id.tvUserNombre);
        tvUserTelefono = findViewById(R.id.tvUserTelefono);

        tvPlaca.setText(placa);

        requestQueue = Volley.newRequestQueue(this);

        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        myLocation = new MyLocation();

        user_markers = new HashMap<>();
        carreras = new HashMap<>();

        locale = new Locale("es", "CO");
        currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        handler = new Handler();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        loadMotoInfo();
    }

    public void loadMotoInfo() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/vehiculos/" + moto_id;

        ajaxRequest.get(url, null, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    placa = response.getString("placa");
                    tvPlaca.setText(placa);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void cambioDisponibilidad(View view) {
        if (disponible.isChecked()) {
            //pacman.setVisibility(View.VISIBLE);
            //pause_interval = false;
            if (!user_markers.isEmpty()) {
                focusAllMarkers();
            } else {
                focusMe();
            }
            updateDisponible(1);
        } else {
            if (selected_carrera != null) {
                Helper.alert(this, "¡Espera!", "Tienes una carrera activa, ¿Quieres cancelarla?", "Cancelar Carrera", "Cerrar", new Helper.ResponseConfirm() {
                    @Override
                    public void onAccept(DialogInterface dialog, int id) {
                        updateEstadoCarrera("C", selected_carrera.getId());
                        clearMap();
                        updateDisponible(0);
                    }

                    @Override
                    public void onCancel(DialogInterface dialog, int id) {
                        // Todo bien :)
                        disponible.setChecked(true);
                    }
                });
            } else {
                clearMap();
                updateDisponible(0);
            }
        }
    }

    private void clearMap() {
        //mMap.clear();
        for(Marker marker : user_markers.values()) {
            marker.setVisible(false);
        }
        if (ruta_origen != null) {
            ruta_origen.remove();
        }
        if (ruta_destino != null) {
            ruta_destino.remove();
        }
        if (marker_destino != null) {
            marker_destino.remove();
        }
        //pause_interval = true;
        //Helper.collapse(rlCarrera);
        rlCarrera.setVisibility(View.GONE);
        focusMe();
    }

    private void updateDisponible(int activo) {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/users/" + user_id;

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("activo", activo + "");
        jsonParams.put("vehiculo_id", moto_id + "");

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.i("updateDisponible", "Actualizó bien");
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (selected_carrera == null) {
                    if (ruta_origen != null) {
                        ruta_origen.remove();
                    }
                    if (ruta_destino != null) {
                        ruta_destino.remove();
                    }
                    if (marker_destino != null) {
                        marker_destino.remove();
                    }
                    if (selected_marker != null) {
                        selected_marker = null;
                    }
                    rlCarrera.setVisibility(View.GONE);
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 30, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    myLocation.setLocation(location);
                    updateMyLocation(myLocation.getLatLng());
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }

        updateData();
    }

    @SuppressLint("MissingPermission")
    private void updateData() {
        if (pause_interval) {
            Log.i("Mot ", "PAUSADO");
            return;
        }

        boolean canAnimateZoom = myLocation.getLocation() == null, updateLocation = false;

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location actualLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));

        if (myLocation.getLocation() != null && (actualLocation.getLatitude() != myLocation.getLocation().getLatitude() || actualLocation.getLongitude() != myLocation.getLocation().getLongitude())) {
            updateLocation = true;
        }

        if (actualLocation != null) {
            myLocation.setLocation(actualLocation);
        }

        if (canAnimateZoom && myLocation.getLatLng() != null) {
            focusMe();
        }

        if (disponible.isChecked() && selected_carrera == null) {
            loadUserMarkers();
        }

        if (selected_carrera != null) {
            updateCarrera();
        }

        if (updateLocation) {
            if (selected_marker != null && disponible.isChecked()) {
                loadRuteTo(selected_marker);
            }
            updateMyLocation(myLocation.getLatLng());
        }
        handler.postDelayed(new Runnable() {
            public void run() {
                updateData();
            }
        }, 3000);
    }

    private void loadUserMarkers() {
        loadCarreras();

        //pacman.setVisibility(View.INVISIBLE);

        if (user_markers.isEmpty()) {
            for (Carrera carrera : carreras.values()) {
                user_markers.put(carrera.getId(), mMap.addMarker(carrera.getOrigenMarker()));
            }
        } else if (!carreras.isEmpty()) {
            ArrayList<Integer> removes = new ArrayList();
            for (Map.Entry<Integer, Marker> entry : user_markers.entrySet()) {
                Carrera carrera = carreras.get(entry.getKey());
                Marker marker = entry.getValue();
                if (carrera == null) {
                    marker.remove();
                    if (selected_marker.equals(marker)) {
                        rlCarrera.setVisibility(View.GONE);
                    }
                    removes.add(entry.getKey());
                } else {
                    if (!marker.getPosition().equals(carrera.getOrigenMarker().getPosition())) {
                        marker.setPosition(carrera.getOrigenMarker().getPosition());
                    }
                }
            }
            for (int i : removes) {
                user_markers.remove(i);
            }
            for (Carrera carrera : carreras.values()) {
                if (user_markers.get(carrera.getId()) == null) {
                    user_markers.put(carrera.getId(), mMap.addMarker(carrera.getOrigenMarker()));
                }
            }
        } else {
            Toast.makeText(this, "No hay carreras disponibles", Toast.LENGTH_SHORT).show();
            for (Marker marker: user_markers.values()) {
                marker.remove();
            }
            user_markers.clear();
            zoom_bounds = true;
            clearMap();
        }

        if (selected_marker == null) {
            zoom_bounds = true;
        }

        if (zoom_bounds && !user_markers.isEmpty()) {
            focusAllMarkers();
            zoom_bounds = false;
        } else if (user_markers.isEmpty()) {
            focusMe();
        }
    }

    private void loadCarreras() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/carreras/activas";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", Integer.toString(user_id));

        ajaxRequest.get(url, null, new AjaxRequest.ResponseArray() {
            @Override
            public void onSuccess(JSONArray response) {
                try {
                    carreras = new HashMap<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject co = response.getJSONObject(i);
                        Carrera carrera = new Carrera();
                        carrera.setId(co.getInt("id"));
                        carrera.setUserId(co.getInt("user_id"));
                        carrera.setOrigenLat(co.getDouble("origen_lat"));
                        carrera.setOrigenLng(co.getDouble("origen_lng"));
                        carrera.setOrigenDir(co.getString("origen_dir"));
                        carrera.setDestinoLat(co.getDouble("destino_lat"));
                        carrera.setDestinoLng(co.getDouble("destino_lng"));
                        carrera.setDestinoDir(co.getString("destino_dir"));
                        carrera.setPrecio(co.getInt("precio"));
                        carrera.setEstado(co.getString("estado"));
                        JSONObject usuario = co.getJSONObject("usuario");
                        User user = new User();
                        user.setNombres(usuario.getString("nombres"));
                        user.setCelular(usuario.getString("celular"));
                        user.setEmail(usuario.getString("email"));
                        carrera.setUser(user);
                        MarkerOptions origen_marker = Helper.createMarker(carrera.getOrigenLat(), carrera.getOrigenLng(), "Origen", R.drawable.marker_user_origen);
                        carrera.setOrigenMarker(origen_marker);
                        MarkerOptions destino_marker = Helper.createMarker(carrera.getDestinoLat(), carrera.getDestinoLng(), "Destino", R.drawable.marker_user_destino);
                        carrera.setDestinoMarker(destino_marker);
                        carreras.put(carrera.getId(), carrera);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateMyLocation(LatLng latLng) {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/users/" + user_id;

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("lat", Double.toString(latLng.latitude));
        jsonParams.put("lng", Double.toString(latLng.longitude));

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.i("updateMyLocation", "Actualizó bien");
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (myLocation.getLocation() == null) {
            Toast.makeText(this, "No se ha determinado la ubicación del dispositivo, intente de nuevo.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (marker.equals(marker_destino)) {
            marker.showInfoWindow();
            return false;
        } else {
            btnAceptar.setVisibility(View.VISIBLE);
            btnCancelar.setVisibility(View.GONE);
            loadRuteTo(marker);
        }
        return true;
    }

    private void loadRuteTo(Marker marker) {
        String url;
        builder = LatLngBounds.builder();

        if (selected_carrera != null && !con_pasajero) {
            url = getGooleRouteUrl(myLocation.getLatLng(), selected_marker.getPosition());
            loadDirections(url, false);
            builder.include(myLocation.getLatLng());
            builder.include(selected_marker.getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
        } else if (con_pasajero) {
            url = getGooleRouteUrl(myLocation.getLatLng(), selected_carrera.getDestinoMarker().getPosition());
            loadDirections(url, true);
            builder.include(myLocation.getLatLng());
            builder.include(selected_carrera.getDestinoMarker().getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
        } else {
            url = getGooleRouteUrl(myLocation.getLatLng(), marker.getPosition());
            loadDirections(url, false);

            Carrera carrera = carreras.get(Helper.getKeyByValue(user_markers, marker));

            if (carrera != null) {

                selected_marker = marker;

                if (marker_destino != null) {
                    marker_destino.remove();
                }

                tvUserNombre.setText(carrera.getUser().getNombres());
                tvUserTelefono.setText(carrera.getUser().getCelular());

                marker_destino = mMap.addMarker(carrera.getDestinoMarker());

                url = getGooleRouteUrl(marker.getPosition(), marker_destino.getPosition());
                loadDirections(url, true);

                tvPrecio.setText(currencyFormatter.format(carrera.getPrecio()));

                //Helper.expand(rlCarrera);
                rlCarrera.setVisibility(View.VISIBLE);

                builder.include(selected_marker.getPosition());
                builder.include(marker_destino.getPosition());
                builder.include(myLocation.getLatLng());
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
            }
        }
        //marker_destino.showInfoWindow();
        // Abrir info usuario, tiempo, distancia, costo.
    }

    private void loadDirections(String url, final boolean destino) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray routes = response.getJSONArray("routes");
                            if (routes.length() > 0) {
                                JSONObject route = routes.getJSONObject(0);
                                JSONArray legs = route.getJSONArray("legs");
                                JSONObject leg = legs.getJSONObject(0);
                                JSONObject distance = leg.getJSONObject("distance");
                                String distancia = distance.getString("text");
                                JSONObject duration = leg.getJSONObject("duration");
                                String duracion = duration.getString("text");
                                String direccion_origen = leg.getString("start_address");
                                String direccion_destino = leg.getString("end_address");
                                JSONArray steps = leg.getJSONArray("steps");

                                PolylineOptions polylineOptions = new PolylineOptions();

                                ArrayList<LatLng> puntos = new ArrayList<>();

                                for (int i = 0; i < steps.length(); i++) {
                                    JSONObject step = steps.getJSONObject(i);
                                    JSONObject polyline = step.getJSONObject("polyline");
                                    String points = polyline.getString("points");
                                    List points_decoded = Helper.decodePolyline(points);
                                    for (Object latLng : points_decoded) {
                                        puntos.add((LatLng) latLng);
                                    }
                                }

                                polylineOptions.addAll(puntos);
                                polylineOptions.color(R.color.colorAccent);
                                polylineOptions.geodesic(true);

                                if (destino) {
                                    if (ruta_destino != null) {
                                        ruta_destino.remove();
                                    }
                                    ruta_destino = mMap.addPolyline(polylineOptions);

                                    tvOrigen.setText(direccion_origen.replace(", Cartagena, Bolívar, Colombia", ""));
                                    tvDestino.setText(direccion_destino.replace(", Cartagena, Bolívar, Colombia", ""));
                                    tvRecorrido.setText(distancia + " ~" + duracion);
                                } else {
                                    if (ruta_origen != null) {
                                        ruta_origen.remove();
                                    }
                                    ruta_origen = mMap.addPolyline(polylineOptions);
                                    if (selected_carrera == null) {
                                        ruta_origen.setVisible(false);
                                    } else {
                                        tvRecorrido.setText(distancia + " ~" + duracion);
                                    }
                                }

                                if (selected_carrera != null) {
                                    int distancia_metros = distance.getInt("value");
                                    if (distancia_metros <= 30 && selected_carrera.getEstado().equals("E")) {
                                        updateEstadoCarrera("B", selected_carrera.getId());
                                        selected_carrera.setEstado("B");
                                        Toast.makeText(GoogleMapsActivity.this, "Pasajero a bordo", Toast.LENGTH_SHORT).show();
                                        con_pasajero = true;
                                        marker_destino = mMap.addMarker(selected_carrera.getDestinoMarker());
                                        loadRuteTo(marker_destino);
                                        Log.i("Mot", selected_carrera.toString());
                                    }
                                }

                                //marker.setSnippet("Distancia: " + distancia + "\nDuración: " + duracion);
                                //marker.showInfoWindow();
                            } else {
                                Toast.makeText(GoogleMapsActivity.this, "No se encontraron rutas", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    public void aceptarCarrera(View view) {
        selected_carrera = carreras.get(Helper.getKeyByValue(user_markers, selected_marker));
        selected_carrera.setEstado("E");
        ruta_origen.setVisible(true);
        ruta_destino.setVisible(false);
        marker_destino.setVisible(false);
        btnAceptar.setVisibility(View.GONE);
        btnCancelar.setVisibility(View.VISIBLE);
        //rlCAccept.setVisibility(View.VISIBLE);
        for (Marker marker : user_markers.values()) {
            if (!marker.equals(selected_marker)) {
                marker.setVisible(false);
            }
        }
        builder = LatLngBounds.builder();
        builder.include(selected_marker.getPosition());
        builder.include(myLocation.getLatLng());
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
        ((TextView) findViewById(R.id.tvRecorridoTitulo)).setText(R.string.llegada);
        loadRuteTo(selected_marker);
        updateEstadoCarrera("E", selected_carrera.getId()); // En curso
        //updateDisponible(0); // Si voy por un pasajero, estoy inactivo
    }

    public void cancelarCarrera(View view) {
        final Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.cancelar))) {
            button.setText(R.string.seguro);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    button.setText(R.string.cancelar);
                }
            }, 3000);
        } else {
            con_pasajero = false;
            if (selected_marker != null) {
                selected_marker = null;
            }
            if (marker_destino != null) {
                marker_destino.remove();
                marker_destino = null;
            }
            if (ruta_origen != null) {
                ruta_origen.remove();
                ruta_origen = null;
            }
            if (ruta_destino != null) {
                ruta_destino.remove();
                ruta_destino = null;
            }
            rlCarrera.setVisibility(View.GONE);
            //user_markers.remove(selected_carrera.getId());
            if (!user_markers.isEmpty()) {
                focusAllMarkers();
            } else {
                focusMe();
            }
            updateEstadoCarrera("C", selected_carrera.getId()); //Cancelada
            //updateDisponible(1);
            selected_carrera = null;
            button.setText(R.string.cancelar);
            button.setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tvRecorridoTitulo)).setText(R.string.recorrido);
            btnAceptar.setVisibility(View.VISIBLE);
        }
    }

    public void updateEstadoCarrera(String estado, int id_carrera) {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/carreras/" + id_carrera;

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("estado", estado);

        if (estado.equals("E")) {
            jsonParams.put("conductor_id", Integer.toString(user_id));
            jsonParams.put("vehiculo_id", moto_id + "");
        }

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                //Toast.makeText(GoogleMapsActivity.this, "Carrera activa", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void focusAllMarkers() {
        builder = LatLngBounds.builder();
        for (Marker marker : user_markers.values()) {
            marker.setVisible(true);
            builder.include(marker.getPosition());
        }
        builder.include(myLocation.getLatLng());
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
    }

    private void focusMe() {
        if (myLocation.getLatLng() != null && mMap != null)
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation.getLatLng(), 15));
    }

    public void callUser(View view) {
        Uri number = Uri.parse("tel:" + ((TextView) view).getText());
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    private String getGooleRouteUrl(LatLng origen, LatLng destino) {
        String str_origen = "origin=" + origen.latitude + "," + origen.longitude;
        String str_destino = "destination=" + destino.latitude + "," + destino.longitude;
        String sensor = "sensor=false";
        String lang = "languaje=es";
        String key = "key=" + getResources().getString(R.string.google_maps_key);
        String params = str_origen + "&" + str_destino + "&" + sensor + "&" + lang + "&" + key;
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + params;
        return url;
    }

    public void goMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(selected_carrera != null) {
            Helper.alert(this, "¡Espera!", "Tienes una carrera activa, ¿Quieres cancelarla?", "Cancelar Carrera", "Cerrar", new Helper.ResponseConfirm() {
                @Override
                public void onAccept(DialogInterface dialog, int id) {
                    updateEstadoCarrera("C", selected_carrera.getId());
                    updateDisponible(0);
                    GoogleMapsActivity.super.onBackPressed();
                }

                @Override
                public void onCancel(DialogInterface dialog, int id) {
                    // Todo bien :)
                    disponible.setChecked(true);
                }
            });
        } else {
            if (disponible.isChecked()) {
                updateDisponible(0);
                disponible.setChecked(false);
            }
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        pause_interval = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        pause_interval = false;
        updateData();
        super.onResume();
    }

    @Override
    protected void onStop() {
        pause_interval = true;
        if (selected_carrera != null) {
            updateEstadoCarrera("C", selected_carrera.getId());
        }
        if (disponible != null && disponible.isChecked()) {
            updateDisponible(0);
            disponible.setChecked(false);
        }
        super.onStop();
    }

    public void cerrarSesion(View view) {
        Helper.alert(this, "Cerrar Sesión", "Estás seguro que quieres cerrar la sesión?", "Si", "No", new Helper.ResponseConfirm() {
            @Override
            public void onCancel(DialogInterface dialog, int id) {
                // Seguimos
            }

            @Override
            public void onAccept(DialogInterface dialog, int id) {
                if (selected_carrera != null) {
                    updateEstadoCarrera("C", selected_carrera.getId());
                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("user_id");
                editor.remove("user_nombres");
                editor.remove("access_token");
                editor.remove("token");
                editor.commit();
                Intent intent = new Intent(GoogleMapsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void updateCarrera() {
        String url = getResources().getString(R.string.API_URL) + "/api/carreras/" + selected_carrera.getId();
        ajaxRequest.get(url, null, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String estado = response.getString("estado");
                    if (selected_carrera == null)
                        return;
                    selected_carrera.setEstado(estado);
                    if (estado.equals("R")) {
                        clearMap();
                        con_pasajero = false;
                        selected_carrera = null;
                        Toast.makeText(GoogleMapsActivity.this, "Carrera finalizada con éxito, gracias por tu servicio", Toast.LENGTH_SHORT).show();
                    } else if (estado.equals("X")) {
                        clearMap();
                        selected_carrera = null;
                        con_pasajero = false;
                        Toast.makeText(GoogleMapsActivity.this, "La carrera fue cancelada por el usuario", Toast.LENGTH_SHORT).show();
                    } else {
                        selected_marker.setPosition(new LatLng(response.getDouble("origen_lat"), response.getDouble("origen_lng")));
                        loadRuteTo(selected_marker);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
