package com.sistemas.driver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sistemas.driver.auth.LoginActivity;
import com.sistemas.driver.auth.SingupActivity;

public class MainActivity extends AppCompatActivity {

    private Button btn_ingreso, btn_registro;

    private SharedPreferences sharedPreferences;
    private String access_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() > 0) {
            goNext();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openLoginActivity(View view) {
        Intent login = new Intent(this, LoginActivity.class);
        startActivity(login);
    }

    public void  openSingUpActivity(View view) {
        Intent registro = new Intent(this, SingupActivity.class);
        startActivity(registro);
    }

    private void goNext() {
        Intent intent;
        String editado = sharedPreferences.getString("editado", "SI");
        if (editado.equals("SI")) {
            intent = new Intent(this, SelectMotoActivity.class);
        } else {
            intent = new Intent(this, EditVehiculoActivity.class);
        }
        startActivity(intent);
    }
}
